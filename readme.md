#  <font color=green> 课程转移到海豚知道 小乙运维杂货铺售卖 </font>
# 购买方式
```shell
购课咨询v【moxiaoyue1007】，可以优惠改价，其余勿扰
43个课程在海豚知道，购买课程后送简历优化，职业规划
8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控
1对1直播答疑等课程也可以看下面的git链接
https://gitee.com/ning1875/devops-guidebook
---
海豚知道购买方式
1. 抖音商城里搜索学习有方店铺，课程在店铺里面
2. 微信打开海豚知道小程序，搜小乙运维杂货铺(这里最全)
3. 视频号搜小乙运维杂货铺，橱窗课程上架中
4. 需要哪个课程 直接找小助理发你链接
5. 快手，微信公众号，小红书课程陆续上架中


```

# 微信打开海豚知道小程序 搜小乙运维杂货铺(这里课程最全)

![img.png](海豚知道/海豚知道小程序.png)

# 8模块大运维平台

![img.png](海豚知道/8模块大运维平台-海豚.png)

## [微信购买地址](https://h5.htknow.com/#/pages/courseInfo/seriesDetail/seriesDetail?id=7491588)
## 8模块大运维平台-微信扫码地址
- ![image](海豚知道/微信-8模块大运维平台开发-go-vue-k8s-cicd-服务树.png)

## 8模块大运维平台-抖音小程序购买
- ![image](海豚知道/抖音-8模块大运维平台开发-go-vue-k8s-cicd-服务树.png)

## 8模块大运维平台-快手小程序购买
- ![image](海豚知道/快手-8模块大运维平台开发-go-vue-k8s-cicd-服务树.png)

# 机器学习gpu训练job平台go+vue3高级运维开发
## [微信购买地址](https://h5.htknow.com/#/pages/courseInfo/seriesDetail/seriesDetail?id=7491683)

## 机器学习gpu训练job平台go+vue3高级运维开发-微信扫码地址
- ![image](海豚知道/微信-机器学习gpu训练job平台go+vue3高级运维开发.png)

## 机器学习gpu训练job平台go+vue3高级运维开发-抖音小程序购买
- ![image](海豚知道/抖音-机器学习gpu训练job平台go+vue3高级运维开发.png)

## 机器学习gpu训练job平台go+vue3高级运维开发-快手小程序购买
- ![image](海豚知道/快手-机器学习gpu训练job平台go+vue3高级运维开发.png)


# aiInfra-大模型-aiOnK8s-gpu-离线训练-volcano调度
## [微信购买地址](https://h5.htknow.com/#/pages/courseInfo/seriesDetail/seriesDetail?id=7491683)

![img.png](海豚知道/aiInfra-大模型-aiOnK8s-gpu-离线训练-volcano调度.png)
https://h5.htknow.com/#/pages/courseInfo/seriesDetail/seriesDetail?id=7486974




#  <font color=green> 2月19日学浪停止运营 后续无法购买小乙老师高级运维开发课程 有需要抓紧联系小助理要优惠购先屯下来 </font>

- ![image](2月19日学浪停止运营 后续无法购买小乙老师高级运维开发课程 有需要抓紧联系小助理要优惠购先屯下来/学浪停止服务.png)
- ![image](2月19日学浪停止运营 后续无法购买小乙老师高级运维开发课程 有需要抓紧联系小助理要优惠购先屯下来/学浪停止服务2.png)

#  <font color=red> 2.19前购买的课程可以一直观看 ：抓紧找小助理屯下课程 </font>


![img.png](aaa.png)
![img.png](2月19日学浪停止运营 后续无法购买小乙老师高级运维开发课程 有需要抓紧联系小助理要优惠购先屯下来/2.19前购买的课程可以一直观看.png)


#  <font color=red> 购课优惠请找小助理，拍完改价格后付款 </font>

- ![image](pic/助理vx.jpg)


# 全新重磅课程  机器学习gpu训练作业平台go+vue3高级运维开发课程 [购买链接: 机器学习gpu训练作业平台go+vue3高级运维开发课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3728211125523841239&origin_type=604)
- [购买链接: 机器学习gpu训练作业平台go+vue3高级运维开发课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3728211125523841239&origin_type=604)
- ![image](pic/机器学习平台开发.png)

```shell
本门课程的定位
带你们开发机器学习平台：
如果运维想做ai k8s 大模型去拍 aiOnK8s那个
这个就是运维开发怎么入行 ai行业：
就是招聘中的必备技能

挑选一些关键词
一台GPU服务器 代表算力
和一台电脑 代表开发环境
深度学习框架：Caffe和TensorFlow
linux的操作系统
显卡驱动 版本
Nvidia专用的并行计算框架CUDA
环境可移植性：容器镜像
标注：照片没有打标
模型开始训练过程中提到了：日志
可视化工具(Tensorboard)
模型发布成一个可供其他service调用的RESTful API(gRPC)

1.11 课程的形式和注意事项
录播
交付物：开发机器学习平台
后端 golang
前端 vue3:vben-admin
k8s技术栈：
volcano
Prometheus
argo-workflow
Informer
gpu NVIDIA设备插件，V10 P40 4090
课程先预售：由于学浪后面不能售卖课程
在新规实施之前购买可以一直学习
```
## ![img.png](pic/模型训练需要开发什么.png)
## [购买链接: 机器学习gpu训练作业平台go+vue3高级运维开发课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3728211125523841239&origin_type=604)

# 机器学习平台开发第二版本文档前后端代码打包发给vip学员，并且已涨价1千，犹豫等来的只能是更高的价格
- 课程超过200集 ![img.png](pic/课程超过200集.png)

# k8s运维开发岗中的面试题解析：运维能力+开发能力
> <font color=red> 热门的问题： 总结热门问题：基本都在小乙老师40个k8s/prometheus/cicd/golang实战运维开发课程中 </font>


| 热门问题                  | 实际想考察/最佳回答体现能力                                                              | 相关课程                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
|-----------------------|-----------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 公司内部k8s应用生命周期怎么管理     | k8s容器从编译，打镜像，部署到集群中 <br/>具备维护cicd/harbor/argocd<br/>或者自研cicd平台对接k8s         | [11_【持续更新】8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604)                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| 能不能go+vue3开发web平台     | 多模块的交互设计，开发能力   ``                                                            |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| k8s多集群管理怎么限制研发操作自己的服务 | 8模块服务树给k8s提供鉴权能力                                                            | [11_【持续更新】8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604)                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| k8s灰度发现实现的原理          | 灰度operator开发，promote插件原理                                                    | [ argocd高级运维k8s多集群helm管理+源码解读+argoRollouts ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3705006759409221871&origin_type=604)                                                                                                                                                                                                                                                                      [11_【持续更新】8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604)                                         |
| k8s-operator开发能力      | kubebuilder 框架熟悉程度  <br/>operator实战开发项目                                     | [老operator和crd实战开发 日志和cicd-助你成为k8s专家](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595280078524815572&origin_type=604)    <br/>[【新课程】k8s运维开发之crd+operator实战之ansible节点池管理+agent-ds开发](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3707613983008751865)                                                                                                                                                                [11_【持续更新】8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) |
| gpu集群怎么调度             | 1.区分训练还是推理<br/> 2.整卡调度还是gpu虚拟化方案<br/> 3.单pod任务还是多pod                        | [ 【新课程】aiInfra大模型aiOnK8s-gpu训练-volcano调度-cilium-ebpf源码解析 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3696623728575250607&origin_type=604)                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| gpu集群坏卡管理             | 1.坏卡监控怎么实现<br/> 2.多集群自动guard守卫                                              | [07_go运维开发实战之k8s多集群自动守卫自愈组件k8s-cluster-guard](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3594937597765513838&origin_type=604)                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| gpu集群资源利用率            | 1.调度层面的在离混部方案<br/>2.用户作业资源利用率画像+webhook动态修改                                 | [ 【持续更新】在线离线混部-潮汐调度开发-k8s大集群资源利用率提升 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3680148583925023059&origin_type=604) <br/> [05_k8s-webhook动态准入源码解读和实战运维开发课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3668080939218633019&origin_type=604)                                                                                                                                                                                                                                                                                                                          |
| cpu集群资源利用率            | 1.超卖方案<br/> 2.调度器framework开发之真实负载调度器                                        | [01_k8s运维大师课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3598135830213973305&origin_type=604)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| k8s多集群监控系统设计          | 1.多集群采集(operator)<br/>2. 监控存储在哪里<br/> 3.有没有开发过web-portal的prometheus监控告警配置平台 | [ 【新课程】基于k8s-thanos的Prometheus监控实战-调优-源码解读-二次开发](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3698145454832353583&origin_type=604)                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| k8s集群中pod怎么被外部访问到     | 1.k8s集群网关apisix<br/>2. 多分支泳道 <br/>3. k8s结合服务发现consul等                       | [04_ingress_k8s流量网关 apisix 高级运维开发课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3624109749701883834&origin_type=604)                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| 处理过哪些有状态服务onK8s       | 1. 有状态服务的存储、副本通信<br/> 2. operator源码                                         | [ 【第8模块】数据库管理平台【mysqlOnK8s】【主从 备份 读写】 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3686654675488211174&origin_type=604)                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| kubelet运行原理，k8s源码能力   | 节点宕机、容器临时存储、calico网络vxlan ipip、csi插件ep ap sp、容器僵尸进程、4种解决镜像拉取失败方案、etcd备份还原、cgroupv2和slab异常内存                                     | [【持续更新】从实际问题入手 k8s新源码解读 丰富的实验+开发样例 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3669946874917421381&origin_type=604)|


# 最新优惠活动展示
#  <font color=red> 有效期【1.1-3.1】 活动来了：小乙老师假期40个运维/开发课程大放价  </font>


## 下面是为了大家在假期学习 小乙老师设置的各种优惠券
| 优惠券 | 课程名称                                              | 优惠券链接                                  |
|-----|---------------------------------------------------|----------------------------------------|
| 立减1000 | [持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控-DB-任务执行grpc | https://haohuo.jinritemai.com/views/coupon/obtain?coupon_meta_id=mWRHjkTenziuJgj |
| 立减89 | golang实战开发课程之pipeline流水线工具                        | https://haohuo.jinritemai.com/views/coupon/obtain?coupon_meta_id=JAUvikWAeRXCmgn |
| 立减79| 【小乙老师强烈推荐的 运维开发年轻人的第一个golang实战练手项目 使用cs架构编写一个k8s中的网络探测daemonset 】 | https://haohuo.jinritemai.com/views/coupon/obtain?coupon_meta_id=GpzrKACtOpikHHb |
| 立减119 | 【【k8s运维开发必备】 10个webhook实战开发项目 】                   | https://haohuo.jinritemai.com/views/coupon/obtain?coupon_meta_id=pkZDYOUQHErVSUs |
| 立减230 | k8s网关运维平台高级课程 apisix ingress实战和源码解读课程             | https://haohuo.jinritemai.com/views/coupon/obtain?coupon_meta_id=hgkucbkLQgIiWzc |
| 立减251 | go运维开发实战之k8s多集群自动守卫自愈组件k8s               cluster-guard | https://haohuo.jinritemai.com/views/coupon/obtain?coupon_meta_id=mWROlIbhqyStJgj |
| 立减599 | k8s运维大师课程，k8s运维开发项目实战，大厂集群调优经验分享                  | https://haohuo.jinritemai.com/views/coupon/obtain?coupon_meta_id=smuVEyUDUbDXRrW |
| 立减499 | 基于k8s  thanos的Prometheus监控实战  调优 源码解读  二次开发       | https://haohuo.jinritemai.com/views/coupon/obtain?coupon_meta_id=ULIKxWplFkvrGdP |
| 立减699 | 【持续更新】从实际问题入手 k8s新源码解读 丰富的实验+开发样例                 | https://haohuo.jinritemai.com/views/coupon/obtain?coupon_meta_id=smuVxvoLcoYwkrW |
| 立减699| aiOnK8s    gpu训练volcano调度 == cilium       ebpf源码解析| https://haohuo.jinritemai.com/views/coupon/obtain?coupon_meta_id=gcZLStxEIXdlhXK  |
| 立减399| 老的operator开发                                      | https://haohuo.jinritemai.com/views/coupon/obtain?coupon_meta_id=KQimzWfQsOJHNE | 
| 立减129| argocd高级运维k8s多集群helm管理+源码解读+argoRollouts          | https://haohuo.jinritemai.com/views/coupon/obtain?coupon_meta_id=pbowzzwexQukHs |
| 立减189| 8模块大运维平台的底座                                       | https://haohuo.jinritemai.com/views/coupon/obtain?coupon_meta_id=EOGPVWzNCpLdwDS | 
| 立减901| 【第8模块】数据库管理平台【mysqlOnK8s】【主从 备份 读写】               | https://haohuo.jinritemai.com/views/coupon/obtain?coupon_meta_id=vCjZIfFOobgELw |
| 立减256| k8s运维开发operator+crd开发之基于ansible的托管节点池             | https://haohuo.jinritemai.com/views/coupon/obtain?coupon_meta_id=mWNxyjAsjEYYPgj |





## 【抓紧 领券-购课-学习吧】

# <font color=red> 如何购买小乙老师40个运维进阶课程 </font>
- 课程在抖音店铺: 使用你常用的抖音账号登陆购买
- [点击这个链接购买：[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604)

- [购买完观看方式： 电脑端下载抖音课堂(原学浪)](https://www.xuelangapp.com/download)
- 使用你购买课程的抖音账号扫码或者短信登陆登陆。就可以学习
- 代码和文档我发给你，冒号前面就是压缩包密码
————————————————————————
- 最新课程可以关注抖音店铺<小乙运维杂货铺>，或者小乙老师 gitee 
- https://gitee.com/ning1875/devops-guidebook

##  <font color=red> 【其实买课前不用加微信，你购买完之后我看到订单会主动加你手机号微信的】实在想咨询就 确认要买课了后再咨询，别没事就加
> 想好了马上要购买课程再加vx咨询 `如果不买课不要加 1天内没买课就删除，杜绝白嫖`


- ![image](pic/xy.png)


> qq群 634305681
- ![image](pic/qq.png)


# <font color=red>  100个k8s-golang开源项目使用和源码解读   </font>
## [01 100个k8s-golang开源项目解读之descheduler](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3708726918380650768&origin_type=604)
- ![image](pic/课程卖点/卖点-descheduler.png)
- 【快速学习+负担低】
- 讲解descheduler的作用
- 配置使用descheduler的常见插件
- descheduler源码解读
- 效果：
    - 使你对k8s资源利用率、污点、亲和性有更深刻的理解
    - 调度问题，如何重新调度
    - 对于k8s运维开发者来说很多库函数
        - 如何分类节点
        - 如何分类pod
        - 如何驱逐pod
        - 插件注册
        - 插件配置
        - interface编码


# golang+vue3实现8模块golang大运维平台前后端全部代码
- 课程介绍
```shell

【课程形式】2000集录播教程视频(持续更新)+直播答疑
【自己一人用golang+vue3实现8模块golang大运维平台前后端全部代码】
【后端golang代码4万行】【60+张mysql表】
【8模块详情如下】
模块01-前后端底座
模块02-服务树和CMDB
模块03-自助工单
模块04-任务执行中心-grpc-server/agent
模块05-prometheus监控平台
模块06-k8s多集群和APP管理
模块07-cicd平台和灰度发布
模块08-数据库和SQL管理平台
----
学习前的门槛：golang基础+前端0基础即可
```
- ![image](pic/8模块大运维平台-海报01.jpeg)
- ![image](pic/飞书卡片.png)
- ![image](pic/cicd_img_23.png)
- ![image](pic/ddl工单详情.png)
- ![image](pic/sql校验.png)


> 如何购买
- 课程在抖音店铺: 使用你常用的抖音账号登陆购买
- [点击这个链接购买：[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604)



# 主推课程购课链接和介绍视频


# 【新课程】k8s运维开发之crd+operator实战之ansible节点池管理+agent-ds开发
- [【新课程】k8s运维开发之crd+operator实战之ansible节点池管理+agent-ds开发](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3707613983008751865)
- ![image](pic/课程卖点/卖点-k8s-operator开发之节点池-节点配置管理ansible.png)
- [【全新课程】 k8s-operator开发节点池之托管containerd 观察监听端口的变化【k8s运维有手就行的】](https://v.douyin.com/ikyqsNQQ/ )
- kubebuilder 开发crd+operator实战教程
- 开发节点池管理节点功能
- 开发节点托管资源监控agent
- golang集成ansible-playbook
  【抖音观看地址】 https://v.douyin.com/ikyqsNQQ/
- [【新课上架，限时优惠3天，优惠价650】](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3707613983008751865&origin_type=604)
- [【同时老的operator开发课程设置优惠价格 只需 791】](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3707613983008751865&origin_type=604)



# 【新课程】aiInfra大模型aiOnK8s-gpu训练-volcano调度-cilium-ebpf源码解析
- ![image](pic/课程卖点/卖点-aiInfra-大模型-aiOnK8s-gpu-离线训练-volcano调度.png)
  [ 【新课程】aiInfra大模型aiOnK8s-gpu训练-volcano调度-cilium-ebpf源码解析 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3696623728575250607&origin_type=604)


> 新课程gpu和ebpf课程涉及开源项目的源码解读
 ![img.png](pic/gpu和ebpf课程涉及开源项目的源码解读.png)

> 购买链接


基于k8s-thanos的Prometheus监控实战-调优-源码解读-二次开发

| 开发类型                                                                   | k8s的扩展点                                                                                                                                                             | 效果演示或简介                                                 | 学成后的结果                                                                      |
|------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------|-----------------------------------------------------------------------------|
| 【新课程】aiInfra大模型aiOnK8s-gpu训练-volcano调度-cilium-ebpf源码解析                 | [ 【新课程】aiInfra大模型aiOnK8s-gpu训练-volcano调度-cilium-ebpf源码解析 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3696623728575250607&origin_type=604)  | [效果演示或简介](https://www.bilibili.com/video/BV1kT42167d2) | 赶上大模型 ai-gpu训练的风口，末班车                                                       |  
| 【新课程】基于k8s-thanos的Prometheus监控实战-调优-源码解读-二次开发                          | [ 【新课程】基于k8s-thanos的Prometheus监控实战-调优-源码解读-二次开发](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3698145454832353583&origin_type=604)  | [效果演示或简介](https://www.bilibili.com/video/BV1KVvCe2Evu/) | k8s prometheus-operator监控 thanos<br/>                                            |  
| 【持续更新】8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控-数据库 <br/> web白屏+抽象k8s应用+整合cicd | [11_【持续更新】8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604)                | [效果演示或简介](https://www.bilibili.com/video/BV1BG411B7Qr/) | vue3.0<br/>k8s-client-go使用<br/>抽象k8s对象<br/>打通cicd灰度发布                       |  
| 1对1 私有解决方案，不限问题，自由答疑，带你写代码分析问题                                         | [ 【VIP1对1视频直播连麦】解决你真实工作问题 带你开发并提供源码 自由提问 手把手带你学习进步 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3690461008750576095&origin_type=604)        | https://www.bilibili.com/video/BV1Us421T77a/            | https://www.bilibili.com/video/BV1QH4y1V7pY                                 |  
| 在线离线混部-潮汐调度开发-k8s大集群资源利用率提升                                            | [ 【第8模块】数据库管理平台【mysqlOnK8s】【主从 备份 读写】 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3686654675488211174&origin_type=604)                     | https://www.bilibili.com/video/BV17E4m1R7Ym/            | https://www.bilibili.com/video/BV1QH4y1V7pY                                 |  
| 在线离线混部-潮汐调度开发-k8s大集群资源利用率提升                                            | [ 【持续更新】在线离线混部-潮汐调度开发-k8s大集群资源利用率提升 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3680148583925023059&origin_type=604)                       | [效果演示或简介](https://www.bilibili.com/video/BV13H4y1p7p6/) | https://www.bilibili.com/video/BV1QH4y1V7pY                                 |  
| 实战问题+源码解读+二次开发                                                         | [01_【持续更新】从实际问题入手 k8s新源码解读 丰富的实验+开发样例 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3669946874917421381&origin_type=604)                     | [效果演示或简介](https://www.bilibili.com/video/BV13H4y1p7p6/) | containerd<br/>calico源码                                                     |  
| k8s主要组件源码解读                                                            | [02_k8s全组件源码讲解和底层原理分析三合一 助力你成为k8s专家](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595279694125268143&origin_type=604)                        | [效果演示或简介](https://www.bilibili.com/video/BV1XJ4m1W7Wr/) | 组件源码                                                                        |  
| Daemonset开发                                                            | [03_k8s中的网络探测吧，作为写golang的一个小的实战项目](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3594920679889365281&origin_type=604)                          | [效果演示或简介](https://www.bilibili.com/video/BV1mt4y1K71G/) | Prometheus-exporter开发<br/>golang-cs架构开发<br/>网络探测                            |
| ingress集群网关                                                            | [04_ingress_k8s流量网关 apisix 高级运维开发课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3624109749701883834&origin_type=604)                        | [效果演示或简介](https://www.bilibili.com/video/BV1qB4y1G7Kf/) | ingress和apisix使用<br/>ingress-nginx控制器源码<br/>apisix二开<br/>用go开发自己的ingress控制器 |
| webhook                                                                | [05_k8s-webhook动态准入源码解读和实战运维开发课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3668080939218633019&origin_type=604)                           | [效果演示或简介](https://www.bilibili.com/video/BV1Tx421f7Hw/) | 8个k8s-webhook开发实战案例<br/>tls证书更新问题                                           |
| 调度器扩展                                                                  | [06_k8s二次开发之基于真实负载的调度器](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595143844292868583&origin_type=604)                                     | [效果演示或简介](https://www.bilibili.com/video/BV1qB4y1G7Kf/) | PrometheusSdk使用<br/>k8s调度框架源码理解<br/>真实负载和集群利用率                              |
| 多集群管理 故障自愈                                                             | [07_go运维开发实战之k8s多集群自动守卫自愈组件k8s-cluster-guard](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3594937597765513838&origin_type=604)               | [效果演示或简介](https://www.bilibili.com/video/BV1QV4y1g7za/) | 多集群管理<br/>常见k8s集群问题和自愈手段                                                    |  
| operator开发                                                             | [08_k8s-operator和crd实战开发 助你成为k8s专家](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595280078524815572&origin_type=604)                         | [效果演示或简介](https://www.bilibili.com/video/BV1cv4y1371X/) | kube-builder使用<br/>基于informer的调谐<br/>workQueue使用                            |
| 企业级实战问题开发                                                              | [09_k8s运维大师课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3598135830213973305&origin_type=604)                                              | [效果演示或简介](https://www.bilibili.com/video/BV11B4y1k7LB/) | k8s生产集群企业级调优方案<br/>10个k8s中实战运维开发项目                                          |
| web白屏操作(简单)                                                            | [10_k8s管理运维平台实战前端vue后端golang](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3596266896661552169&origin_type=604)                               | [效果演示或简介](https://www.bilibili.com/video/BV1QV4y1g7za/) | vue2.0<br/>k8s-client-go使用<br/>web平台化                                       |  
| web白屏+抽象k8s应用+整合cicd                                                   | [11_【持续更新】8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604)                | [效果演示或简介](https://www.bilibili.com/video/BV1BG411B7Qr/) | vue3.0<br/>k8s-client-go使用<br/>抽象k8s对象<br/>打通cicd灰度发布                       |  






# 【给k8s和go开发新手的】推荐几个运维golang运维开发练手项目

| 课程名称                                                                                                                                                                                           | 推荐理由                                                                                 | 介绍视频                                                                    |
|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------|-------------------------------------------------------------------------|
| [【小乙老师强烈推荐的 运维开发年轻人的第一个golang实战练手项目 使用cs架构编写一个k8s中的网络探测daemonset-1】 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3594920679889365281&origin_type=605&pd_hide_footer=1) | 200多块钱的超值golang运维开发小课程<br/>cs架构 prometheus exporter 开发<br/>还有daemonset 这价格这内容还要啥自行车  | https://b23.tv/xk5Ycad                                                  |
| [ 【小乙老师给k8s运维强烈推荐的k8s网关运维平台高级课程 apisix ingress实战和源码解读课程】](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3624109749701883834&origin_type=605&pd_hide_footer=1)             | k8s运维必须会维护ingress-nginx/apisix等网关<br/>实战网关配置调优<br/>源码解读<br/>还会用golang实现自己的ingress控制器 | https://b23.tv/AOxAr3q <br/>https://www.bilibili.com/video/BV1RN41167Df |
| [【k8s运维开发必备】 webhook在k8s运维开发中的重要性-哔哩哔哩】](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3668080939218633019&origin_type=605&pd_hide_footer=1)                              | 难度低，有hook框架，适合刚学会go想做k8s运维开发新手<br/>10个实战webhook案例帮助你了解k8s开发思路<br/> 日常k8s运维必备技能       | https://b23.tv/PVL0nUL <br/> https://b23.tv/lMctMxK                     |                                                 |




# k8s在离混布-抄袭调度-动态感知高级运维开发效果图

![image](pic/k8s在离混布-抄袭调度-动态感知高级运维开发效果图.png)
![image](pic/课程卖点/卖点-k8s在离混部-潮汐调度golang开发实战.png)

> 购买链接 点击下面这个
- [ 【持续更新】在线离线混部-潮汐调度开发-k8s大集群资源利用率提升 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3680148583925023059&origin_type=604)



#  <font color=red>【新课程】【VIP1对1视频直播连麦】解决你真实工作问题 带你开发并提供源码 自由提问 手把手带你学习进步 </font>
> 购买链接
- [ 【VIP1对1视频直播连麦】解决你真实工作问题 带你开发并提供源码 自由提问 手把手带你学习进步 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3690461008750576095&origin_type=604)

## 进行总结：
- 长达1年的1对1直播视频连麦
- 自由提任何k8s/监控/cicd/golang问题
- 带你分析并解决你搞不定的问题
- 帮助你开发代码并给你源码
- 系统的由运维专家带你提升


> 课程大纲
- [VIP1对1视频直播连麦 自由提问 解决生产问题](pic/大纲pic/1v1新课程的头脑风暴.md)

> 课程卖点
- ![image](pic/1v1课程/img_5.png)


# 小乙老师30多个运维、运维开发课程在抖音店铺 ：搜索 `小乙运维杂货铺`
> 小乙老师社交账号
- [抖音：小乙运维杂货铺](https://www.douyin.com/user/MS4wLjABAAAAjipE-5odpRWa2jw6tRYH5IOuvUEjAauAYMm-a80khfo)
- [b站：小乙运维杂货铺](https://space.bilibili.com/278569661)
- [知乎：小乙运维杂货铺](https://www.zhihu.com/people/lang-zi-yan-qing-yan-xiao-yi-62)
- [快手：小乙运维杂货铺 快手号 ning18751](https://www.douyin.com/user/MS4wLjABAAAAjipE-5odpRWa2jw6tRYH5IOuvUEjAauAYMm-a80khfo)


##  <font color=red> 【其实买课前不用加微信，你购买完之后我看到订单会主动加你手机号微信的】实在想咨询就 确认要买课了后再咨询，别没事就加
> 想好了马上要购买课程再加vx咨询 `如果不买课不要加 1天内没买课就删除，杜绝白嫖`


- ![image](pic/xy.png)


> qq群 634305681
- ![image](pic/qq.png)



# 8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控 简介

> 如何购买
- 课程在抖音店铺: 使用你常用的抖音账号登陆购买
- [点击这个链接购买：[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604)

> 购买完如何观看

| 终端 | 方式     | 备注                     |
|----|--------|------------------------|
| 手机 | 抖音直接观看 | 不推荐，课程需要编码和查看文档，手机无法操作 |
| 电脑 |   电脑端下载[学浪](https://www.xuelangapp.com/download)  <br/>![img.png](pic/学浪.png)   |     使用你购买的抖音账号扫扫码登陆                   |

> 购买后如何启动服务
- [【运维兄弟购买完小乙老师的8模块golang大运维平台课程 微信群里拿到代码怎么快速启动服务】](https://www.bilibili.com/video/BV1N64y1p7iR)

## <font color=red> 8模块大运维平台课程简介 </font>
> 进度追踪

| 购课链接                                                                                                                              | 分析进阶视频                                                                                                                    | 
|-----------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------|
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [目标1000集[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控价格优惠](https://www.bilibili.com/video/BV1ku411T7pB)                         |
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [【golang-vue3-8模块大运维平台视频突破800集了-目标1500进发 37张mysql表】](https://www.bilibili.com/video/BV1WQ4y1b7fD)                         |
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [[【突破900集 模块5Prometheus监控已完成 12个页面 15张db表 3万行后端golang代码 k8s火热开发中 等你来解锁-1】 ](https://www.bilibili.com/video/BV1gC4y1y7Qf)  |
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [【第281期 当年定下的目标 8模块golang大运维平台1500集目标已实现 世上无难事只怕有心人 有志青年速来购买 即将第二次涨价】](https://www.bilibili.com/video/BV1bA4m1V7XL)       |  
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [【第271期 从golang代码go-mod引用外部依赖库之多 来说明下8模块go大运维平台课程有多么丰富 一劳永逸解决你的运维平台开发问题-1】 ](https://www.bilibili.com/video/BV1j2421c7ac) |  


#    [点击这个链接购买：[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604)


> 8模块成果样例

| 购课链接                                                                                                                              | 分析进阶视频                                                                                                                               | 
|-----------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------|
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [模块01 基础底座](https://www.bilibili.com/video/BV16F411r7aP/)                                                                            |  
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [模块02 服务树和cmdb 解绑ecs资源 - 1of3](https://www.bilibili.com/video/BV1wK4y1w7b6/)                                                         |  
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [模块02 服务树echarts资产统计](https://www.bilibili.com/video/BV1sj41187tQ/)                                                                  |  
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [模块03 工单审批流和表单设计器](https://www.bilibili.com/video/BV1V8411y7Aw/)                                                                     |  
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [模块04-grpc-agent和任务执行中心演示01](https://www.bilibili.com/video/BV1qw411X74d/)                                                           |
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [模块05-prometheus平台 【突破900集 模块5Prometheus监控已完成 12个页面 15张db表 3万行后端golang代码 k8s火热开发中 等你来解锁-1】 ](https://www.bilibili.com/video/BV1gC4y1y7Qf) |
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [模块05-prometheus平台【第251期 开发Prometheus运维监控平台都有哪8个核心资源 为什么要用池来管理alertmanager和采集器】](https://www.bilibili.com/video/BV1TN4y1m7pZ)                        |
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [模块05-prometheus平台【【开发Webhook告警处理模块】为何要构建SendGroup的cache】](https://www.bilibili.com/video/BV1Jw411T7bv)|
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [模块05-prometheus平台【如何使用golang去管理Prometheus和alertmanager构建监控平台-1】 ](https://www.bilibili.com/video/BV1E94y137Ge)|
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [模块06-k8s【k8s模块运维开发之容器日志 02 解决tailLog的问题】](https://www.bilibili.com/video/BV1kN411V7fJ)|
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [模块06-k8s【golang运维开发写k8s多集群管理平台 为什么要面向2种人群  权限和功能有什么实现思路】 ](https://www.bilibili.com/video/BV1BG411B7Qr)                               |
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [模块06-k8s【第260期  用go开发运维平台之k8s模块开发过半了 给你们看看部分截图效果 坚持做一件事总会有收获 你还在犹豫吗 有志青年速来】 ](https://www.bilibili.com/video/BV1XN4y147ph)            |
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [模块07-cicd【【8模块golang大运维平台开发】cicd流水线对接k8s接口失败了 看看运维专家如何解决】 ]( https://www.bilibili.com/video/BV1L2421c75L)                             |
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [模块07-cicd【8模块golang大运维平台 cicd模块完整的cicd流水线go代码-ci检测-harbor镜像-k8s-app展示 - 1of3】 ](https://www.bilibili.com/video/BV16e411n7H3)          |
| [[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604) | [模块07-cicd【第282期 运维专家必备go运维开发之cicd模块结合k8s和服务树实现容器灰度发布】 ](https://www.bilibili.com/video/BV11C411s7Ro)                                  |

> 如何购买
- 课程在抖音店铺: 使用你常用的抖音账号登陆购买
- [点击这个链接购买：[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604)


## 代码行数统计和购课前置技能
![image](pic/统计-2.png)



## 8模块大运维平台截图
### 8模块大运维平台截图之 【模块1-底座】

![image](pic/新增用户来验证审批组过滤逻辑.png)
![image](pic/12.10.jwt临期刷新问题.png)

### 8模块大运维平台截图之 【模块2-服务树和cmdb】
![image](./pic/8模块_资产统计.png)
![image](./pic/8模块_头部打印服务树节点表示a.b.c.d前端展示.png)

###    [点击这个链接购买：[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604)

### 8模块大运维平台截图之 【模块3-工单】
![image](pic/工单评论.png)
![image](pic/表单设计2.png)
![image](pic/工单通知全.png)


### 8模块大运维平台截图之 【模块4-任务执行中心和grpc-agent】
![image](pic/并发度滚动执行.png)
![image](pic/grpc的联调.png)

![image](pic/任务kill.png)
![image](pic/切换用户执行.png)


### 8模块大运维平台截图之  【模块5-Prometheus监控 模块】
![image](pic/飞书卡片.png)
![image](pic/告警屏蔽.png)
![image](pic/k8s采集.png)
![image](pic/http服务发现标签.png)

###    [点击这个链接购买：[持续更新]8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604)


### 8模块大运维平台截图之 【模块6-k8s-容器日志】
![image](pic/img_12.png)
![image](pic/img_13.png)
![image](pic/k8s_img_2.png)

> k8s单机详情
![image](pic/k8s单机详情.png)
![image](pic/img_5.png)
![image](pic/img_7.png)
![image](pic/img.png)

> k8s模块后端
![image](pic/img_20.png)
![image](pic/img_21.png)
![image](pic/img_22.png)
![image](pic/img_23.png)
![image](pic/img_24.png)

### 8模块大运维平台截图之  【模块7-cicd 模块】
![image](pic/img_18.png)
![image](pic/cicd_img_16.png)
![image](pic/cicd_img_21.png)
![image](pic/cicd_img_22.png)
![image](pic/cicd_img_23.png)



##  <font color=red> 【其实买课前不用加微信，你购买完之后我看到订单会主动加你手机号微信的】实在想咨询就 确认要买课了后再咨询，别没事就加
> 想好了马上要购买课程再加vx咨询 `如果不买课不要加 1天内没买课就删除，杜绝白嫖`


- ![image](pic/xy.png)


> qq群 634305681
- ![image](pic/qq.png)



# k8s中的运维开发都需要做些什么
> 按照k8s的扩展点

| 开发类型                 | k8s的扩展点                                                                                                                                               | 效果演示或简介                                                                                | 学成后的结果                                                                      |
|----------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------|-----------------------------------------------------------------------------|
| 实战问题+源码解读+二次开发       | [01_【持续更新】从实际问题入手 k8s新源码解读 丰富的实验+开发样例 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3669946874917421381&origin_type=604)       | [效果演示或简介](https://www.bilibili.com/video/BV13H4y1p7p6/)  | containerd<br/>calico源码                                                     |  
| k8s主要组件源码解读     | [02_k8s全组件源码讲解和底层原理分析三合一 助力你成为k8s专家](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595279694125268143&origin_type=604)          | [效果演示或简介](https://www.bilibili.com/video/BV1XJ4m1W7Wr/)  | 组件源码                                                                        |  
| Daemonset开发          | [03_k8s中的网络探测吧，作为写golang的一个小的实战项目](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3594920679889365281&origin_type=604)            | [效果演示或简介](https://www.bilibili.com/video/BV1mt4y1K71G/)  | Prometheus-exporter开发<br/>golang-cs架构开发<br/>网络探测                            |
| ingress集群网关          | [04_ingress_k8s流量网关 apisix 高级运维开发课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3624109749701883834&origin_type=604)          | [效果演示或简介](https://www.bilibili.com/video/BV1qB4y1G7Kf/)  | ingress和apisix使用<br/>ingress-nginx控制器源码<br/>apisix二开<br/>用go开发自己的ingress控制器 |
| webhook              | [05_k8s-webhook动态准入源码解读和实战运维开发课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3668080939218633019&origin_type=604)             | [效果演示或简介](https://www.bilibili.com/video/BV1Tx421f7Hw/)  | 8个k8s-webhook开发实战案例<br/>tls证书更新问题                                           |
| 调度器扩展                | [06_k8s二次开发之基于真实负载的调度器](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595143844292868583&origin_type=604)                       | [效果演示或简介](https://www.bilibili.com/video/BV1qB4y1G7Kf/)  | PrometheusSdk使用<br/>k8s调度框架源码理解<br/>真实负载和集群利用率                              |
| 多集群管理 故障自愈           | [07_go运维开发实战之k8s多集群自动守卫自愈组件k8s-cluster-guard](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3594937597765513838&origin_type=604) | [效果演示或简介](https://www.bilibili.com/video/BV1QV4y1g7za/)  | 多集群管理<br/>常见k8s集群问题和自愈手段                                                    |  
| operator开发           | [08_k8s-operator和crd实战开发 助你成为k8s专家](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595280078524815572&origin_type=604)           | [效果演示或简介](https://www.bilibili.com/video/BV1cv4y1371X/)           | kube-builder使用<br/>基于informer的调谐<br/>workQueue使用                            |
| 企业级实战问题开发            | [09_k8s运维大师课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3598135830213973305&origin_type=604)                                | [效果演示或简介](https://www.bilibili.com/video/BV11B4y1k7LB/)  | k8s生产集群企业级调优方案<br/>10个k8s中实战运维开发项目                                          |
| web白屏操作(简单)          | [10_k8s管理运维平台实战前端vue后端golang](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3596266896661552169&origin_type=604)                 | [效果演示或简介](https://www.bilibili.com/video/BV1QV4y1g7za/)  | vue2.0<br/>k8s-client-go使用<br/>web平台化                                       |  
| web白屏+抽象k8s应用+整合cicd | [11_【持续更新】8模块大运维平台开发-go-vue-k8s-cicd-服务树-监控](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3641191333189820599&origin_type=604)                  | [效果演示或简介](https://www.bilibili.com/video/BV1BG411B7Qr/)  | vue3.0<br/>k8s-client-go使用<br/>抽象k8s对象<br/>打通cicd灰度发布                       |  


# 【新课程】k8s运维开发之crd+operator实战之ansible节点池管理+agent-ds开发
- [【新课程】k8s运维开发之crd+operator实战之ansible节点池管理+agent-ds开发](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3707613983008751865)
- ![image](pic/课程卖点/卖点-k8s-operator开发之节点池-节点配置管理ansible.png)
- [【全新课程】 k8s-operator开发节点池之托管containerd 观察监听端口的变化【k8s运维有手就行的】](https://v.douyin.com/ikyqsNQQ/ )
- kubebuilder 开发crd+operator实战教程
- 开发节点池管理节点功能
- 开发节点托管资源监控agent
- golang集成ansible-playbook
  【抖音观看地址】 https://v.douyin.com/ikyqsNQQ/
- [【新课上架，限时优惠3天，优惠价650】](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3707613983008751865&origin_type=604)
- [【同时老的operator开发课程设置优惠价格 只需 791】](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3707613983008751865&origin_type=604)





# 【新课程】【argocd高级运维k8s多集群helm管理+源码解读+argoRollouts】
> 购买链接
- [ argocd高级运维k8s多集群helm管理+源码解读+argoRollouts ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3705006759409221871&origin_type=604)

- [17.9 运维专家必备之cicd蓝绿灰度发布结合k8s中的apisix网关达成无损平滑发布-1](https://www.bilibili.com/video/BV1m9Hfe4EPn/)
- [17.9 运维专家必备之cicd蓝绿灰度发布结合k8s中的apisix网关达成无损平滑发布-2](https://v.douyin.com/ihCLj1MJ/)

> 课程卖点
- ![image](pic/卖点-argocd课程.png)
- [argocd源码解析之repo的redis缓存哨兵和单点模式](https://v.douyin.com/ihCLj1MJ/)


# 【新课程】aiInfra大模型aiOnK8s-gpu训练-volcano调度-cilium-ebpf源码解析
- ![image](pic/课程卖点/卖点-aiInfra-大模型-aiOnK8s-gpu-离线训练-volcano调度.png)


> 购买链接
- [ 【新课程】aiInfra大模型aiOnK8s-gpu训练-volcano调度-cilium-ebpf源码解析 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3696623728575250607&origin_type=604)

- ![image](pic/gpu新品榜单.png)


> 课程卖点 ：大模型风口下面为数不多 运维能喝的汤
- ![image](pic/gpu课程规划.png)

# 【新课程】 基于k8s-thanos的Prometheus监控实战-调优-源码解读-二次开发

> 课程卖点 ：
- ![iage](pic/课程卖点/卖点-thanos.png)
- ![iage](pic/thanos卖点.png)



## 购买连接
https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3698145454832353583&origin_type=604





# 【新课程】【第8模块】数据库管理平台【mysqlOnK8s】【主从 备份 读写】
> 购买链接
- [ 【第8模块】数据库管理平台【mysqlOnK8s】【主从 备份 读写】 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3686654675488211174&origin_type=604)

> 课程卖点
- ![image](pic/ddl工单详情.png)
- ![image](pic/sql校验.png)



# 【新课程】【VIP1对1视频直播连麦】解决你真实工作问题 带你开发并提供源码 自由提问 手把手带你学习进步
> 购买链接
- [ 【VIP1对1视频直播连麦】解决你真实工作问题 带你开发并提供源码 自由提问 手把手带你学习进步 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3690461008750576095&origin_type=604)

> 课程卖点
- ![image](pic/1v1大纲.png)


# 【新课程】[持续更新]在线离线混部-潮汐调度开发-k8s大集群资源利用率提升
> 购买链接
- [ 【持续更新】在线离线混部-潮汐调度开发-k8s大集群资源利用率提升 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3680148583925023059&origin_type=604)

> 课程卖点
- ![image](pic/在离混部卖点.png)
- ![image](pic/课程卖点/卖点-k8s在离混部-潮汐调度golang开发实战.png)




# 【新课程】k8swebhook-运维开发

> 购买链接 
- [07_k8s-webhook动态准入源码解读和实战运维开发课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3668080939218633019&origin_type=604)

> [开篇介绍视频地址](https://www.bilibili.com/video/BV1Tx421f7Hw/) 

> 截图效果
- ![image](pic/webhook/img_7.png)
- ![image](pic/webhook/img_8.png)
- ![image](pic/webhook/img_9.png)
- ![image](pic/webhook/img_10.png)
- ![image](pic/webhook/img_11.png)

# 【新课程】k8s新源码解读课程
> 购买链接
- [【持续更新】从实际问题入手 k8s新源码解读 丰富的实验+开发样例 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3669946874917421381&origin_type=604)

> 课程卖点
- ![image](pic/新源码解读卖点pic/img_6.png)
- ![image](pic/课程卖点/卖点-新源码解读.png)

> 截图效果
- ![image](pic/new/k8s源码-new.png)


# 【新课程】8模块底座课程 ，作为使用8模块大运维平台开发的前菜
> 购买链接
- [课程名称: golang运维平台底座vue3-gin-gorm-ts-csbin-casbin](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3671053279934152737&origin_type=604)



#  <font color=red>【直播！！】运维进阶训练营</font>
- ![运维训练营直播.png](pic/运维训练营直播.png)
- 介绍
```shell
【小乙老师运维直播进阶训练营火热招生中】
【小班授课，直播授课】
【目前我给出一个课程内容大纲模板，实际内容会按照报名同学的实际情况或需求进行调整！！ 】
 01 比如学员普遍基础不好，就多讲基础
 02 比如学员基础好，那就多讲源码和开发
【费用和周期】
  费用1w以上，具体看报名情况
  周期3月以上，具体看报名情况
【预计开班时间 2024年-8月17日 周六】
【报名截止日期 2024年-8月14日 周三】
有志青年私聊跟我说，拉你进需求群
```
- [购买链接](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3700867764001571041&origin_type=604)

## 大纲模板，实际内容会按照报名同学的实际情况或需求进行调整
- [运维直播训练营可能的大纲01.md](运维直播训练营可能的大纲01.md)
  ![image](pic/直播训练营img.png)


# 【掌握最新的运维开发技术 】运维目前3大核心方向 想成为和我一样的专家么 经验分享给你

## 目前运维的3个火热的方向 : k8s、监控、cicd

- [分析视频链接](https://www.bilibili.com/video/BV1pL4y1A7ST)
- 无论是否直接维护开发这3大类工具，都必须要求我们对这些比较熟悉

> 为什么现在k8s相关岗位火热，给钱多

- 因为k8s的出现给公司省钱
- ![image](pic/new/img.png)
- 大幅降低公司的机器成本和开发效率成本
- 所以市场上对k8s人才的需求是巨大的

> 到这里我们可以得到1个结论就是

- 对运维来说k8s已经是必备技能 而非加分项
- [分析视频链接](https://www.bilibili.com/video/BV1Mg41127s3/)

> 同时监控和cicd可以作为k8s的附属项目

> 那么作为运维的我们 怎么进阶到这3大核心方向呢

## 听我给你们规划一个完整的学习路线

## k8s要先入门 会运维操作

- ![image](pic/new/img_1.png)
### [k8s零基础入门课程链接](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3594912111454759880&origin_type=604)

- [k8s零基础入门课程链接](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3594912111454759880&origin_type=604)

### 大致包含下面几部分

- 01 集群安装和认识集群组件
- 02 安装必要的控制台/监控/日志等组件
- 03 kubectl 常见操作
- 04 容器相关操作如exec 查看日志等
- 05 常见控制器(dep/ds/sts/job)的操作
- 06 k8s 存储对象源码解读
- 07 k8s网络相关Service和ingress

## 然后这时候老板让你帮助 业务上云 上k8s，所以你需要学习一下怎么做cicd，怎么写dockerFile ，怎么部署到k8s集群中

### cicd实战

| 学习方向                                                                 | 分析进阶视频        | 备注  | 
|----------------------------------------------------------------------|---------|-----------|
| [01_tekton全流水线实战和pipeline运行原理源码解读](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595279771367522483&origin_type=604) | [地址](https://www.bilibili.com/video/BV13P4y1Z7Xv/)  | 	     |  

- ![image](pic/new/img_2.png)
## 在维护了一段时间k8s集群后发现 prometheus监控的基础并不牢靠

- 需要从基础到进阶好好学习一下prometheus

### prometheus监控从入门到专家之路

| 学习方向                                                                              | 分析进阶视频  | 备注  | 
|-----------------------------------------------------------------------------------|---------|-----|
| [01_prometheus零基础入门，grafana基础操作，主流exporter采集配置](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595140472760349913&origin_type=604) | [地址](https://www.bilibili.com/video/BV1814y1e73y/)  | 	    |     |  
| [02_prometheus全组件配置使用、底层原理解析、高可用实战](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3598138690670622282&origin_type=604)             | [地址](https://www.bilibili.com/video/BV1oZ4y1f7au/)  |      |  
| [03_kube-prometheus和prometheus-operator实战和原理介绍](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3597588013493936532&origin_type=604) | [地址](https://www.bilibili.com/video/BV1LR4y1L7jV/)  | 	 |  
| [04_prometheus-thanos使用和源码解读](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3597591765223345001&origin_type=604)                   | [地址](https://www.bilibili.com/video/BV1814y1e73y/)  | 	   |  
| [05_prometheus源码讲解和二次开发](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3594913505213599660&origin_type=604)                        | [地址](https://www.bilibili.com/video/BV1hS4y1m73Q/)  | 	    |  
| [06_prometheus监控k8s的实战配置和原理讲解，写go项目暴露业务指标](https://ke.qq.com/course/5837369)                        | [地址](https://www.bilibili.com/video/BV1mW4y1Y7AU/)  | 	    |  





## 然后发现缺乏对k8s源码的了解 导致排查不了复杂的问题：所以必须要恶补一下go语言知识

### golang运维开发之从0基础到运维平台

| 学习方向                                                                                                                                          | 分析进阶视频  | 备注   | 
|-----------------------------------------------------------------------------------------------------------------------------------------------|---------|------|
| [01_golang基础课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3598136416477008227&origin_type=604)                       | [地址](https://www.bilibili.com/video/BV1WT411M7Gh/)  |      |  
| [02_golang运维平台实战，服务树,日志监控，任务执行，分布式探测](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3598136543119864780&origin_type=604) | [地址](https://www.bilibili.com/video/BV14T4y1k7oo)  |      |  
| [03_golang运维开发实战课程之k8s巡检平台](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595143953697090709&origin_type=604)           | [地址](https://www.bilibili.com/video/BV1Ad4y1r7C4/)  |      |  
| [04_golang实战开发课程之pipeline流水线工具](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3614475679007175557&origin_type=604)           | [地址](https://www.bilibili.com/video/BV1Ad4y1r7C4/)  |      |  

## 有了go基础之后就可以 畅快的阅读k8s源码

### k8s从零基础入门到专家到运维大师

- ![image](pic/new/e6.png)

| 学习方向                                                                                                                                        | 分析进阶视频  | 备注  | 
|----------------------------------------------------------------------------------------------------------------------------------------------|---------|-----|
| [01_k8s零基础入门实战](https://ke.qq.com/course/5829699)                                                                                            | [地址](https://www.bilibili.com/video/BV1Mt4y1P7bL/)  |       |  
| [02_k8s全组件源码讲解和底层原理分析三合一 助力你成为k8s专家](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595279694125268143&origin_type=604) | [地址](https://www.bilibili.com/video/BV1or4y1877p/)  |       |  
| [03_k8s节点宕机pod检测工具和k8s探针代码解读](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3617798475296701560&origin_type=604) | [地址](https://www.bilibili.com/video/BV1or4y1877p/)  |       |  

- ![image](pic/new/img_3.png)
## 有了k8s源码的基础的你，这时候开始摩拳擦掌 想做一些k8s开发工作了


### k8s 开发篇

| 学习方向                                                                                                                                                  | 分析进阶视频  | 备注  | 
|-------------------------------------------------------------------------------------------------------------------------------------------------------|---------|------------|
| [01_k8s运维大师课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3598135830213973305&origin_type=604)                                | [地址](https://www.bilibili.com/video/BV11B4y1k7LB/)  |     |  
| [02_k8s-operator和crd实战开发 助你成为k8s专家](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595280078524815572&origin_type=604)           | [地址](https://www.bilibili.com/video/BV1cv4y1371X/)  |         |  
| [03_k8s二次开发之基于真实负载的调度器](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595143844292868583&origin_type=604)                       | [地址](https://www.bilibili.com/video/BV1qB4y1G7Kf/)  |       |  
| [04_go运维开发实战之k8s多集群自动守卫自愈组件k8s-cluster-guard](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3594937597765513838&origin_type=604) | [地址](https://www.bilibili.com/video/BV1QV4y1g7za/)  |       |  
| [05_k8s管理运维平台实战前端vue后端golang](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3596266896661552169&origin_type=604)                 | [地址](https://www.bilibili.com/video/BV1QV4y1g7za/)  |       |  
| [06_k8s中的网络探测吧，作为写golang的一个小的实战项目](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3594920679889365281&origin_type=604)            | [地址](https://www.bilibili.com/video/BV1mt4y1K71G/)  |       |  
| [07_k8s-webhook动态准入源码解读和实战运维开发课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3668080939218633019&origin_type=604)            | [地址](https://www.bilibili.com/video/BV1Tx421f7Hw/)  |       |  

### k8s调优
- [01_k8s集群调优小课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3594756712952942291&origin_type=604)
- [02_k8s运维大师课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3598135830213973305&origin_type=604)

# [面试题 k8s和golang面试真题解析 运维开发面试经验分享 linux运维进阶 ](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3600844212968499216&origin_type=604) 


- ![image](pic/new/img_4.png)
## 如果全部学习完成之后就会发现 可以无障碍阅读许多k8s周边的go 开源项目了

## 并且可以 修改其中源码进行二次开发，或者借鉴其中的逻辑自由的开发组件了


## 零基础学习路线：
- 先golang零基础 [01_golang基础课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3598136416477008227&origin_type=604)
- 再k8s零基础 [k8s零基础入门课程链接](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3594912111454759880&origin_type=604)
- 再prometheus零基础  [01_prometheus零基础入门，grafana基础操作，主流exporter采集配置](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595140472760349913&origin_type=604)
- 再k8s中的网络探测吧，作为写golang的一个小的实战项目 [06_k8s中的网络探测吧，作为写golang的一个小的实战项目](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3594920679889365281&origin_type=604)
- 再k8s调优 [01_k8s集群调优小课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3594756712952942291&origin_type=604)
- 再  [06_prometheus监控k8s的实战配置和原理讲解，写go项目暴露业务指标](https://ke.qq.com/course/5837369)
- 再  [03_golang运维开发实战课程之k8s巡检平台](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595143953697090709&origin_type=604)
- 再 [02_k8s纯源码解读课程，助力你变成k8s专家](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595279694125268143&origin_type=604)
> 最后再 做k8s中的二次开发
- [01_k8s运维大师课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3598135830213973305&origin_type=604)                                | [地址](https://www.bilibili.com/video/BV11B4y1k7LB/)  
- [02_k8s-operator和crd实战开发 助你成为k8s专家](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595280078524815572&origin_type=604)           | [地址](https://www.bilibili.com/video/BV1cv4y1371X/)  
- [03_k8s二次开发之基于真实负载的调度器](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595143844292868583&origin_type=604)                       | [地址](https://www.bilibili.com/video/BV1qB4y1G7Kf/) 
- [04_go运维开发实战之k8s多集群自动守卫自愈组件k8s-cluster-guard](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3594937597765513838&origin_type=604) | [地址](https://www.bilibili.com/video/BV1QV4y1g7za/) 
- [05_k8s管理运维平台实战前端vue后端golang](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3596266896661552169&origin_type=604)                 | [地址](https://www.bilibili.com/video/BV1QV4y1g7za/) 


## 有golang基础的想往k8s运维开发进阶走的
- 再k8s中的网络探测吧，作为写golang的一个小的实战项目 [06_k8s中的网络探测吧，作为写golang的一个小的实战项目](https://ke.qq.com/course/5860635)
- [02_golang运维平台实战，服务树,日志监控，任务执行，分布式探测]https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3598136543119864780&origin_type=604)             | [地址](https://www.bilibili.com/video/BV14T4y1k7oo)  

- [01_k8s运维大师课程](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3598135830213973305&origin_type=604)                                | [地址](https://www.bilibili.com/video/BV11B4y1k7LB/)  
- [02_k8s-operator和crd实战开发 助你成为k8s专家](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595280078524815572&origin_type=604)           | [地址](https://www.bilibili.com/video/BV1cv4y1371X/)  
- [03_k8s二次开发之基于真实负载的调度器](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3595143844292868583&origin_type=604)                       | [地址](https://www.bilibili.com/video/BV1qB4y1G7Kf/) 
- [04_go运维开发实战之k8s多集群自动守卫自愈组件k8s-cluster-guard](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3594937597765513838&origin_type=604) | [地址](https://www.bilibili.com/video/BV1QV4y1g7za/) 
- [05_k8s管理运维平台实战前端vue后端golang](https://haohuo.jinritemai.com/ecommerce/trade/detail/index.html?id=3596266896661552169&origin_type=604)                 | [地址](https://www.bilibili.com/video/BV1QV4y1g7za/)  





## 直播答疑sre职业发展规划
- [k8s-prometheus课程答疑和运维开发职业发展规划](https://ke.qq.com/course/5506477)

> 关于白嫖和付费
- 白嫖当然没关系，我已经贡献了很多文章和开源项目，当然还有免费的视频
- 但是客观的讲，如果你能力超强是可以一直白嫖的，可以看源码。什么问题都可以解决
- 看似免费的资料很多，但大部分都是边角料，核心的东西不会免费，更不会有大神给你答疑
- thanos和kube-prometheus如果你对prometheus源码把控很好的话，再加上k8s知识的话就觉得不难了


> 付费后看看大家的反馈
![image](pic/反馈01.png)
![image](pic/反馈02.png)
![image](pic/反馈03.png)
![image](pic/反馈04.png)
![image](pic/反馈05.png)







## 04 免费课程目录导航
- [01 prometheus采集k8s底层原理](prometheus免费课程/01_prometheus适配k8s采集.md)
- [02 k8s监控指标讲解](prometheus免费课程/02_k8s监控指标讲解.md)
- [03 时序监控集群存储m3db](prometheus免费课程/03_时序监控集群存储m3db.md)
- [04 低成本multi_remote_read方案](prometheus免费课程/04_低成本multi_remote_read方案.md)

## 免费课程链接
- 课程链接：[prometheus为了适配k8s监控的改造，高可用时序监控存储实战](https://ke.qq.com/course/3517990?taid=12068265399791142&tuin=361e95b0)
